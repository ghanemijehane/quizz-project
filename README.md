**Présentation**
<br>
Pour ce projet j'ai choisi le thème des desserts qui reflète ma gourmandise.
<br>
<br>
J'ai ajouté plusieurs fonctionnalités : 
<br>

- les images qui défilent en fonction des questions

- des classes css qui change de couleur lorsque la réponse est vraie (vert) ou fausse (rouge)
- décompte des points et affichage du score en temps réel et à la fin

Le site doit être responsive et validé par W3C.

**Compétences utilisées**
<br>
- variables
-  conditions
- tableaux et tableaux d'objets
-  boucles 
- fonctions
- DOM (querySelector / events)

<hr>

**Maquette**
<br>
![wireframe Quizz Project](public/maquette-quizz.png)


[Lien vers la maquette](https://whimsical.com/GFKtXmWpwpb14hacgnShiV
)

<hr>

**Site web quizz**

[Lien vers mon quizz](https://ghanemijehane.gitlab.io/quizz-project/)


