// Mes questions
let questionNo = document.querySelector("#questionNo");
let questionText = document.querySelector("#questionText");

// Mes réponses
let option1 = document.querySelector("#option1");
let option2 = document.querySelector("#option2");
let option3 = document.querySelector("#option3");
let option4 = document.querySelector("#option4");

// Bouton suivant
let nextButton = document.querySelector('#next-btn');

//Tous les h4 de mon quiz (MQCM)
let choice_que = document.querySelectorAll(".choice_que");

let total_correct = document.querySelector("#total_correct");
let next_question = document.querySelector("#next_question");

let end = document.querySelector("#end")
let bloc = document.querySelector('#bloc')
let MQCM = [{
        question: "De quel pays vient ce dessert ?",
        choice1: "Hongrie",
        choice2: "Turquie",
        choice3: "Croatie",
        choice4: "Egypte",
        answer: 1
    },
    {
        question: "De quel pays vient ce dessert ?",
        choice1: "Chine",
        choice2: "Vietnam",
        choice3: "Japon",
        choice4: "Laos",
        answer: 2
    },
    {
        question: "De quel pays vient ce dessert ?",
        choice1: "Luxembourg",
        choice2: "Pays-Bas",
        choice3: "Allemagne",
        choice4: "Belgique",
        answer: 3
    },
    {
        question: "De quel pays vient ce dessert ?",
        choice1: "Russie",
        choice2: "Ukraine",
        choice3: "Nouvelle-Zelande",
        choice4: "Bulgarie",
        answer: 2
    },
    {
        question: "De quel pays vient ce dessert ?",
        choice1: "Mexique",
        choice2: "Espagne",
        choice3: "Portugal",
        choice4: "France",
        answer: 1
    },
    {
        question: "De quel pays vient ce dessert ?",
        choice1: "Portugal",
        choice2: "Italie",
        choice3: "Espagne",
        choice4: "France",
        answer: 0
    },
    {
        question: "De quel pays vient ce dessert ?",
        choice1: "France",
        choice2: "Irlande",
        choice3: "Italie",
        choice4: "Chili",
        answer: 2
    },
    {
        question: "De quel pays vient ce dessert ?",
        choice1: "Iran",
        choice2: "Maroc",
        choice3: "Tunisie",
        choice4: "Jordanie",
        answer: 1
    },
    {
        question: "De quel pays vient ce dessert ?",
        choice1: "Liban",
        choice2: "Inde",
        choice3: "Malaysie",
        choice4: "Cambodge",
        answer: 2
    },
    {
        question: "De quel pays vient ce dessert ?",
        choice1: "Indonésie",
        choice2: "Vietnam",
        choice3: "Laos",
        choice4: "Cambodge",
        answer: 1
    },
]


let index = 0;


// fonction pour afficher mes données questions + réponses
let loadData = () => {
    questionNo.textContent = index + 1 + ". ";
    questionText.textContent = MQCM[index].question;
    option1.textContent = MQCM[index].choice1;
    option2.textContent = MQCM[index].choice2;
    option3.textContent = MQCM[index].choice3;
    option4.textContent = MQCM[index].choice4;

}


let myImage = document.querySelector('.img')
let imageArray = ['image0.jpg', 'image1.jpg', 'image2.jpg', 'image3.jpg', 'image4.jpg',
    'image5.jpg', 'image6.jpg', 'image7.jpg', 'image8.jpg', 'image9.png'
]
let imageIndex = 1;
let imgend = document.querySelector('#end-img')

// event sur le bouton suivant 
nextButton.addEventListener('click', () => {

    // condition qui incrémente l'index pour changer d'image et de question
    if (index !== MQCM.length - 1) {
        index++;
        myImage.setAttribute('src', imageArray[imageIndex]);
        imageIndex++;
        removeActiveClass(choice_que)

        //questions
        loadData();

        // affiche le total des points
        total_correct.textContent = correct + " / " + MQCM.length + " points";
    } else {
        bloc.style.display = 'none';
        myImage.style.display = 'none'
        imgend.style.display = 'block'
        end.textContent = 'Bravo vous avez obtenu ' + correct + " points sur " + MQCM.length


    }

    // boucle qui retire les classes pour déselectionner le background vert ou rouge
    for (i = 0; i <= 3; i++) {
        choice_que[i].classList.remove("green", "red");
        choice_que[i].classList.remove("disabled")



    }
})


// fonction qui retire la classe active quand on séléctionne une réponse
function removeActiveClass(paramClass) {
    paramClass.forEach(removeActive => {
        removeActive.classList.remove("active");

    })

}



choice_que.forEach((choices, choiceNo) => {
    choices.addEventListener("click", () => {
        choices.classList.add("active");
        //check la bonne réponse
        if (choiceNo === MQCM[index].answer) {
            correct++;
            choices.classList.add('green');


        } else {
            correct += 0;
            choices.classList.add('red');
        }
        //stop Compteur


        //Désactive toutes les classes
        for (i = 0; i <= 3; i++) {
            choice_que[i].classList.add("disabled");




        }
    })
});



let correct = 0;

loadData()